# Laboratory 2 – Control of a Continuous Stirred Tank Reactor (CSTR) 
One of the laboratories in «Advanced and Multivariable Control» (AMC) A.Y. 2022/2023

### The phase plane of the system
![alt text](figures/cstr_ol_q1.jpg)

### The phase plane of the closed-loop system stabilized with pole placement controller 
![alt text](figures/cstr_clpp_q3.jpg)


# Included Contents
### Live Scripts:
- [main_cstr.mlx](main.mlx)

### Simulink Models:
- [cstr_cascade_control.slx](cstr_cascade_control.slx)
![alt text](figures/cstr_cascade_control.jpeg)

- [cstr_openloop_response.slx](cstr_openloop_response.slx)
![alt text](figures/cstr_openloop_response.jpeg)

- [cstr_place_control.slx](cstr_place_control.slx)
![alt text](figures/cstr_place_control.jpeg)

- [cstr_place_control_integral.slx](cstr_place_control_integral.slx)
![alt text](figures/cstr_place_control_integral.jpeg)

